import { AutorService } from './../../shared/service/autor.service';
import { LivroService } from './../../shared/service/livro.service';
import { Component, OnInit } from '@angular/core';
import { Livro } from 'src/app/shared/model/livro';

@Component({
  selector: 'app-livros-list',
  templateUrl: './livros-list.component.html',
  styleUrls: ['./livros-list.component.css']
})
export class LivrosListComponent implements OnInit {

  livros: Livro[] = [];
  livro: Livro = new Livro();

  constructor(
    private autoresService: AutorService,
    private livrosService: LivroService) { }

  ngOnInit(): void {
    this.livrosService.getLivros().subscribe(
      (response) => {
        response.forEach((livro: Livro) => {
          this.autoresService.getAutorById(Number(livro.autorId)).subscribe(
            (autor) => { livro.nomeAutor = autor.nome, this.livros.push(livro); }
          );
        });
      }
    );
  }

  preparaDel(id: any): void{
    this.livrosService.getLivroById(id).subscribe(
        (response) => this.livro = response
    );
}

deletar(): void{
    this.livrosService.deleteLivro(this.livro.id).subscribe(
      () => {this.livros = [], this.ngOnInit(); }
    );
}
}
