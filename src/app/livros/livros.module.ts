import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LivrosRoutingModule } from './livros-routing.module';
import { LivrosListComponent } from './livros-list/livros-list.component';
import { LivroFormComponent } from './livro-form/livro-form.component';


@NgModule({
  declarations: [
    LivrosListComponent,
    LivroFormComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    LivrosRoutingModule
  ],
  exports: [
    LivrosListComponent
  ]
})
export class LivrosModule { }
