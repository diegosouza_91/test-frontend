import { LivroFormComponent } from './livro-form/livro-form.component';
import { LivrosListComponent } from './livros-list/livros-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: 'livros-list' , component: LivrosListComponent},
  {path: 'livro-form' , component: LivroFormComponent},
  {path: 'livro-form/:id' , component: LivroFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LivrosRoutingModule { }
