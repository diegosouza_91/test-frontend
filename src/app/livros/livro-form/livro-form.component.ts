import { LivroService } from './../../shared/service/livro.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Autor } from 'src/app/shared/model/autor';
import { AutorService } from 'src/app/shared/service/autor.service';
import { Livro } from 'src/app/shared/model/livro';

@Component({
  selector: 'app-livro-form',
  templateUrl: './livro-form.component.html',
  styleUrls: ['./livro-form.component.css']
})
export class LivroFormComponent implements OnInit {

  alteracao = false;
  livro = new Livro();
  autores: Autor[] = [];

constructor(
  private livrosService: LivroService,
  private activeteRoute: ActivatedRoute,
  private router: Router,
  private autorService: AutorService) { }

formulario = new FormGroup({
  nomeDoLivro : new FormControl('', [Validators.required]),
  autorId : new FormControl('', [Validators.required]),
  preco : new FormControl('', [Validators.required])
  });

ngOnInit(): void {

  this.getAutores();
  this.edicaoLivro();

}

alterar(): void{
  const livro = new Livro();
  livro.id = this.livro.id;
  livro.nomeDoLivro = this.formulario.value.nomeDoLivro;
  livro.autorId = this.formulario.value.autorId;
  livro.preco = this.formulario.value.preco;
  console.log(livro);
  this.livrosService.putLivro(livro).subscribe(
    () => this.router.navigate(['/livros-list'])
  );

}

cadastrar(): void{
  const livro: Livro = this.formulario.value;
  this.livrosService.postLivro(livro).subscribe(
    () => this.router.navigate(['/livros-list'])
  );
}

getAutores(): void{
  this.autorService.getAutores().subscribe(
    (response) => this.autores = response
  );
}

edicaoLivro(): any{
  this.activeteRoute.params.subscribe((params: Params) => {
    if ( params.id !== undefined){
        this.alteracao = true;
        this.livrosService.getLivroById(params.id).subscribe(
            (response) => {

                this.livro = response;

                this.autorService.getAutorById(Number(this.livro.autorId)).subscribe(
                  (autor) => {this.livro.nomeAutor = autor.nome,
                    this.formulario.controls.nomeDoLivro.setValue(this.livro.nomeDoLivro),
                    this.formulario.controls.autorId.setValue(this.livro.autorId),
                    this.formulario.controls.preco.setValue(this.livro.preco);
                  }
                );

                }
            );
        }
  });

}

}
