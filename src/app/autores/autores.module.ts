import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutoresRoutingModule } from './autores-routing.module';
import { AutoresListComponent } from './autores-list/autores-list.component';

import { AutorFormComponent } from './autor-form/autor-form.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AutoresListComponent,
    AutorFormComponent,
  ],
  imports: [
    CommonModule,
    AutoresRoutingModule,
    ReactiveFormsModule
  ], exports: [AutoresListComponent]
})
export class AutoresModule { }
