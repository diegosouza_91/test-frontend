import { AutorService } from './../../shared/service/autor.service';
import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/shared/model/autor';

@Component({
  selector: 'app-autores-list',
  templateUrl: './autores-list.component.html',
  styleUrls: ['./autores-list.component.css']
})
export class AutoresListComponent implements OnInit {

    autores: Autor[] = [];

    autor = new Autor();


  constructor(

      private autorService: AutorService
  ) { }

  ngOnInit(): void {

    this.autorService.getAutores().subscribe(
        (response) => this.autores = response
    );
  }

  preparaDel(id: any): void{
      this.autorService.getAutorById(id).subscribe(
          (response) => this.autor = response
      );
  }

  deletar(): void{
      this.autorService.deleteAutor(this.autor.id).subscribe(
        () => this.ngOnInit()
      );
  }

}
