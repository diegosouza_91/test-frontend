import { AutorFormComponent } from './autor-form/autor-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoresListComponent } from './autores-list/autores-list.component';

const routes: Routes = [
    {path: 'autores-list' , component: AutoresListComponent},
    {path: 'autor-form' , component: AutorFormComponent},
    {path: 'autor-form/:id' , component: AutorFormComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutoresRoutingModule { }
