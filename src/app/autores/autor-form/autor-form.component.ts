import { AutorService } from './../../shared/service/autor.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Autor } from 'src/app/shared/model/autor';

@Component({
  selector: 'app-autor-form',
  templateUrl: './autor-form.component.html',
  styleUrls: ['./autor-form.component.css']
})
export class AutorFormComponent implements OnInit {

    alteracao = false;
    autor = new Autor();

  constructor(
    private activeteRoute: ActivatedRoute,
    private router: Router,
    private autorService: AutorService) { }

  formulario = new FormGroup({
    nome : new FormControl('', [Validators.required])
    });

  ngOnInit(): void {
      this.activeteRoute.params.subscribe((params: Params) => {
            if ( params.id !== undefined){
                this.alteracao = true;
                this.autorService.getAutorById(params.id).subscribe(
                    (response) => {
                        this.autor = response;
                        this.formulario.controls.nome.setValue(this.autor.nome);
                    }
                );
            }
      });
  }

  alterar(): void{
      this.autor.nome = this.formulario.value.nome;
      this.autorService.putAutor(this.autor).subscribe(
          () => this.router.navigate(['/autores-list'])
      );
  }

  cadastrar(): void{
      this.autor.nome = this.formulario.value.nome;
      this.autorService.postAutor(this.autor).subscribe(
          () => this.router.navigate(['/autores-list'])
    );
  }

}
