import { Autor } from 'src/app/shared/model/autor';
export class Livro{
    id?: number;
    nomeDoLivro?: string;
    autorId?: number;
    nomeAutor?: string;
    preco?: number;
}
