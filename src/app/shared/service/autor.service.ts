import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Autor } from '../model/autor';

@Injectable({
  providedIn: 'root'
})
export class AutorService {

private autorURL = environment.url + 'autor';

  constructor(
      private http: HttpClient
  ) { }


  getAutores(): Observable<Autor[]>{
      return this.http.get<Autor[]>(this.autorURL);
  }

  getAutorById(id: number): Observable<Autor>{
      return this.http.get<Autor>(`${this.autorURL}/${id}`);
  }

  postAutor(autor: Autor): Observable<Autor>{
      return this.http.post<Autor>(this.autorURL, autor);
  }

  putAutor(autor: Autor): Observable<Autor>{
    return this.http.put<Autor>(`${this.autorURL}/${autor.id}`, autor );
    }

  deleteAutor(id: any): Observable<Autor>{
    return this.http.delete<Autor>(`${this.autorURL}/${id}`);
  }
}
