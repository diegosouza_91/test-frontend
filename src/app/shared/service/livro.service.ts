import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Livro } from '../model/livro';

@Injectable({
  providedIn: 'root'
})
export class LivroService {

  private livrosURL = environment.url + 'livro';

  constructor(
    private http: HttpClient
    ) { }

    getLivros(): Observable<Livro[]>{
      return this.http.get<Livro[]>(this.livrosURL);
    }

    getLivroById(id: number): Observable<Livro>{
      return this.http.get<Livro>(`${this.livrosURL}/${id}`);
    }

    postLivro(autor: Livro): Observable<Livro>{
      return this.http.post<Livro>(this.livrosURL, autor);
    }

    putLivro(autor: Livro): Observable<Livro>{
      return this.http.put<Livro>(`${this.livrosURL}/${autor.id}`, autor );
    }

    deleteLivro(id: any): Observable<Livro>{
      return this.http.delete<Livro>(`${this.livrosURL}/${id}`);
    }
}
